package carcarrierapp;

import carcarrierapp.exceptions.CarAlreadyPresentException;
import carcarrierapp.exceptions.CarNotFoundException;

import java.time.Year;
import java.util.UUID;

public class CarCarrierApp {

    public static void main(String[] args) throws CarNotFoundException, CarAlreadyPresentException {
        // Cars
        Car ford = new Car("Ford", Year.of(2020), 20000.0, UUID.randomUUID().toString());
        Car audi = new Car("Audi", Year.of(2020), 70000.0, UUID.randomUUID().toString());
        Car bmw = new Car("BMW", Year.of(2020), 50000.0, UUID.randomUUID().toString());
        Car citroen = new Car("Citroen", Year.of(2020), 30000.0, UUID.randomUUID().toString());
        Car renault = new Car("Renault", Year.of(2020), 20000.0, UUID.randomUUID().toString());
        // Dealers
        Dealer floc = new Dealer("Garage floc");
        Dealer monopoly = new Dealer("Garage Monopoly");
        // Truck
        Truck myMasterTruck = new Truck("Chief Master Truck");

        System.out.println("------------------------------------");
        System.out.println("All cars will be loaded in my beautiful truck " + myMasterTruck.getName());
        myMasterTruck.loadCar(ford);
        myMasterTruck.loadCar(audi);
        myMasterTruck.loadCar(bmw);
        myMasterTruck.loadCar(citroen);
        myMasterTruck.loadCar(renault);

        System.out.println("------------------------------------");
        myMasterTruck.travel();

        System.out.println("------------------------------------");
        System.out.println("All cars will be delivered by my beautiful truck " + myMasterTruck.getName());
        Car audiToDeliver = myMasterTruck.deliveredACar(audi.getVehicleIdentificationNumber());
        floc.addToCatalog(audiToDeliver);

        monopoly.addToCatalog(myMasterTruck.deliveredACar(ford.getVehicleIdentificationNumber()));
        monopoly.addToCatalog(myMasterTruck.deliveredACar(bmw.getVehicleIdentificationNumber()));
        monopoly.addToCatalog(myMasterTruck.deliveredACar(citroen.getVehicleIdentificationNumber()));
        monopoly.addToCatalog(myMasterTruck.deliveredACar(renault.getVehicleIdentificationNumber()));

        System.out.println("------------------------------------");
        System.out.println("Who is the richest dealer?");
        System.out.println("Floc has a cash position of $" + floc.computeTreasury());
        System.out.println("Monopoly has a cash position of $" + monopoly.computeTreasury());

        System.out.println("------------------------------------");
        System.out.println("Monopoly sell ford");
        Car carSelled = monopoly.sellACar(ford.getVehicleIdentificationNumber());
        System.out.println(carSelled);

        System.out.println("------------------------------------");
        System.out.println("Who is the richest dealer?");
        System.out.println("Floc has a cash position of $" + floc.computeTreasury());
        System.out.println("Monopoly has a cash position of $" + monopoly.computeTreasury());

    }
}
