package carcarrierapp.exceptions;

public class CarAlreadyPresentException extends Exception {
    public CarAlreadyPresentException(String message) {
        super(message);
    }
}
