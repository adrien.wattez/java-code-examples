package carcarrierapp;

import java.time.Year;

public class Car {
    private String brand;
    private Year yearModel;
    private Double priceModel;
    private final String vehicleIdentificationNumber;

    public Car(String brand, Year yearModel, Double priceModel, String vehicleIdentificationNumber) {
        this.brand = brand;
        this.yearModel = yearModel;
        this.priceModel = priceModel;
        this.vehicleIdentificationNumber = vehicleIdentificationNumber;
    }


    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Double getPriceModel() {
        return priceModel;
    }

    public void setPriceModel(Double priceModel) {
        this.priceModel = priceModel;
    }


    public Year getYearModel() {
        return yearModel;
    }

    public void setYearModel(Year yearModel) {
        this.yearModel = yearModel;
    }

    public String getVehicleIdentificationNumber() {
        return vehicleIdentificationNumber;
    }

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", yearModel=" + yearModel +
                ", priceModel=" + priceModel +
                ", vehicleIdentificationNumber='" + vehicleIdentificationNumber + '\'' +
                '}';
    }
}
