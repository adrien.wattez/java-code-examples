package carcarrierapp;

import carcarrierapp.exceptions.CarAlreadyPresentException;
import carcarrierapp.exceptions.CarNotFoundException;

import java.util.HashMap;
import java.util.Map;

public class Truck {
    private final String name;
    private final Map<String, Car> cars;

    public Truck(String name) {
        this.name = name;
        this.cars = new HashMap<>();
    }

    public String getName() {
        return name;
    }

    public void loadCar(Car car) throws CarAlreadyPresentException {
        if (cars.containsKey(car.getVehicleIdentificationNumber())) {
            throw new CarAlreadyPresentException("car already present in the truck");
        }
        cars.put(car.getVehicleIdentificationNumber(), car);
        System.out.println("Truck[" + this.getName() + "] just loaded " + car.toString());
    }

    public Car deliveredACar(String vehicleNumber) throws CarNotFoundException {
        if (!cars.containsKey(vehicleNumber)) {
            throw new CarNotFoundException("car missing in the truck");
        }
        return cars.remove(vehicleNumber);
    }


    public void travel() {
        System.out.println("I travel the world and the seven seas");
        System.out.println("                     ..-------------------.__\n" +
                "" +
                "" +
                "                   .'_______                  `-.\n" +
                "                  // .-----.\\.--------..--------.\\\n" +
                "                  ||'    __'||        ||   ||   || __\n" +
                "                  ||' .=(_ )|| ====== || ==.|   ||( _)\n" +
                "                  ||'|    \\\\||________||________||//\n" +
                "                  ||'------\\) ,--======\\\\======-._/\n" +
                "              ____||        |/           = =      `-.\n" +
                "    _____________ ||'==.    ||   ......      = =     `-.\n" +
                "   `=============`||        ||_ /////.--.        = =    `. .--.\n" +
                "   |    .---.     ||        |   .----|==|     \\    \\ \\    \\|==|\n" +
                "   | .'       `.  ||        | .'     '--'.    |-.  | |  .-|'--'`. \n" +
                "   .'           \\ ||        .'            `.  |-.'-|=|-'.-|      \\\n" +
                "  /    .-==-.    \\||       /  _.----.       \\ |-.'-|=|-'.-|      |\n" +
                " |  .' .---. `.  |||       .-' .---. `.     | |-.'-|=|-'.-|      |\n" +
                " | / .`.- -.`. \\ '\\_`---- /  .`.- -.`. \\    | '  '-|=|-'  '_____/\n" +
                "[|_|/ /  _  \\ \\|__________| / /  _  \\ \\ `_____.......-----'_____]\n" +
                "    ; : / \\ : ;'----------'; :  / \\  : ;[_____.......-----'; :\n" +
                "    : ; \\_/ ; : \\     / /  ; :  \\_/  ; :          \\ \\     / /\n" +
                "    \\ \\     / /. `- -` .    \\ \\     / /            . `- -` .\n" +
                "     . `- -` .  `-----`      . `- -` .              `-----`\n" +
                "      `-----`                 `-----`");

        System.out.println("Everybody' s looking for something");
    }
}