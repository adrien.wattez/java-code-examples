package carcarrierapp;

import carcarrierapp.exceptions.CarNotFoundException;

import java.util.ArrayList;

public class Dealer {
    private final ArrayList<Car> cars = new ArrayList<>();
    private final String name;

    public Dealer(String name) {
        this.name = name;
    }

    public void addToCatalog(Car car) {
        cars.add(car);
        System.out.println("Dealer[" + this.getName() + "] correctly received " + car.toString());
    }

    public Car sellACar(String vehicleIdentificationNumber) throws CarNotFoundException {
        for (Car car : cars) {
            if (car.getVehicleIdentificationNumber().equals(vehicleIdentificationNumber)) {
                cars.remove(car);
                System.out.println("Dealer[" + this.getName() + "] correctly sold " + car.toString());
                return car;
            }
        }
        throw new CarNotFoundException("Unable to sell this car, this car is not present at the dealership");
    }

    public Double computeTreasury() {
        Double total = 0d;
        for (Car car : cars) {
            total += car.getPriceModel();
        }
        return total;
    }

    public String getName() {
        return name;
    }
}
