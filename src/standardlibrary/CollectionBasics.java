package standardlibrary;

// To use something from another file, it must be imported.
// Most java IDE do this for you.

import java.util.ArrayList;
import java.util.HashMap;


public class CollectionBasics {
    public static void main(String[] args) {
        // Collections are classes. To use them, you need to create an instance.
        // Here we create an ArrayList that can contain Strings. A list is an ordered set of elements, and the elements can later be accessed by their index.
        // The type og the object contained in the collection is specified between the <> characters. This is called a _type parameter_.
        // The type of the variable cheeseILike is ArrayList<String>. It can't contain a value of type ArrayList<Integer>.
        ArrayList<String> cheeseILike = new ArrayList<>();
        cheeseILike.add("Camembert");
        cheeseILike.add("Comté");
        cheeseILike.add("Cheddar");
        // FIXME set & hashset
        cheeseILike.add("Camembert");

        // Using lists often involves loops
        // Note that the index of the first elements 0 and not 1
        for (int i = 0; i < cheeseILike.size(); ++i) {
            System.out.println("I like " + cheeseILike.get(i));
        }

        // With collections, we can use the _forEach loop_
        // This is equivalent to the previous example.
        for (String s : cheeseILike) {
            System.out.println("I like " + s);
        }

        if (cheeseILike.contains("Cheddar")) {
            System.out.println("You must be from england.");
        }

        // Another kind of collection is the Map. Maps contain elements indexed by a key. Then the elements can be accessed by specifying
        // their key.
        // Only one element can be contained in a map for a given key.
        // Here, keys are of type String and Values are of type Integer.
        HashMap<String, Integer> ageOfPeople = new HashMap<>();
        ageOfPeople.put("Paul", 32);
        ageOfPeople.put("Jacques", 23);
        ageOfPeople.put("Simone", 25);
        System.out.println(ageOfPeople.get("Paul"));

        // If we get the value of a key that is absent of the map, we obtain a _null value_.
        System.out.println(ageOfPeople.get("Damian"));

        // Null values are dangerous, so we can specify a default value with the `getOrDefault` method.
        System.out.println(ageOfPeople.getOrDefault("Salomé", 1));
    }
}
