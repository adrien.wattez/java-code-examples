package standardlibrary;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class HttpClientExample {

    public static void main(String[] args) {

        // HttpClient is an abstract class that can't be instantiated directly with the `new` keyword. Instead, you can use the
        // `newHttpClient` factory method to get a default instance.
        HttpClient http = HttpClient.newHttpClient();

        // To create an `HttpRequest` object, you have to use the builder pattern by calling the `newBuilder` static method and then
        // providing parameters like the uri and method.
        HttpRequest simpleApiRequest = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create("https://jsonplaceholder.typicode.com/todos"))
                .build();

        try {
            // The request is sent like this (keep in mind this will block the thread, it may cause performance problems. use the sendAsync
            // method to avoid this)
            HttpResponse<String> response = http.send(simpleApiRequest, HttpResponse.BodyHandlers.ofString());
            System.out.println(response.body());
            System.out.println(response.statusCode());
        } catch (IOException  | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
