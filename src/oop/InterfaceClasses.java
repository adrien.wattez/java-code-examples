package oop;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

// The implements keyword is used to implement an interface.
// The interface keyword is used to declare a special type of class that only contains abstract methods.
// To access the interface methods, the interface must be "implemented" (kinda like inherited) by another class with the implements keyword (instead of extends ).
interface HelloWorld {
    void sayHello();
}

// Your class can implement more than one interface,
// so the implements keyword is followed by a comma-separated list of the interfaces implemented by the class.
class HelloWorldConsole implements HelloWorld {
    String name = "Hello World!";

    @Override
    public void sayHello() {
        System.out.println(name);
    }

    public void byeBye() {
        System.out.println("Bye !");
    }
}

class HelloWorldFile implements HelloWorld {
    private static final File MY_FILE = tmpFile();

    private static File tmpFile() {
        try {
            return File.createTempFile("Hello", ".txt");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void sayHello() {
        try {
            System.out.println("Write Hello in " + MY_FILE.getAbsolutePath());
            FileOutputStream fileOutputStream = new FileOutputStream(MY_FILE);
            fileOutputStream.write("Hello World!".getBytes(StandardCharsets.UTF_8));
            fileOutputStream.close();
        } catch (IOException e) {
            System.err.println("Error");
        }

    }

    public void readContentFile() {
        // The try-with-resources statement is a try statement that declares one or more resources.
        // A resource is an object that must be closed after the program is finished with it.
        // Any object that implements java.lang.AutoCloseable, which includes all objects which implement java.io.Closeable, can be used as a resource.
        // FileInputStream is Autocloseable
        try (FileInputStream fileInputStream = new FileInputStream(MY_FILE);) {
            System.out.println(MY_FILE.getAbsolutePath());
            String content = new String(fileInputStream.readAllBytes(), StandardCharsets.UTF_8);
            System.out.println(content);
        } catch (IOException e) {
            System.err.println("Error");
        }

    }
}

public class InterfaceClasses {
    public static void main(String[] args) {
        System.out.println("Create an HelloWorld Console");
        HelloWorld helloWorldConsole = new HelloWorldConsole();
        helloWorldConsole.sayHello();

        ((HelloWorldConsole) helloWorldConsole).byeBye();

        System.out.println("Create an HelloWorld File");

        HelloWorld helloWorldFile = new HelloWorldFile();
        helloWorldFile.sayHello();

        ((HelloWorldFile) helloWorldFile).readContentFile();
    }
}
