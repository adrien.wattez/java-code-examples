package oop;

// Classes can inherit from one another. To have an inheritance relationship, there must be a _base class_, and another class that _extends_ it.
// A base class doesn't need to have anything special.

class Vehicle {
    // the `protected` visibility means that the attribute is accessible only from within the class an all classes that extends it.
    protected double speed = 0;
    protected String driverName;

    public Vehicle(String driverName) {
        this.driverName = driverName;
    }

    public double getCurrentSpeed() {
        return this.speed;
    }

    public void accelerate() {
        if (this.speed == 0) {
            this.speed = 1;
        }
    }
}

// This class inherits from the `Vehicle` class. we can say that "a car is a Vehicle".
// Instances of the class Car will have all the attributes and methods of the class Car, but also all attributes and methods of the base
// class.
class Car extends Vehicle {
    private double accelerationFactor = 1.1;

    public Car(String driverName) {
        super(driverName);
    }

    public void accelerate() {
        this.speed = this.speed * this.accelerationFactor;
        super.accelerate();
    }
}

// Another class that extends the Vehicle class.
class Motorcycle extends Vehicle {
    protected double accelerationFactor = 1.5;

    public Motorcycle(String driverName) {
        super(driverName);
    }

    public void accelerate() {
        this.speed = this.speed * this.accelerationFactor;
        super.accelerate();
    }

    public void doAControlledSkid() {
        System.out.println("Style points !");
        this.speed = 0;
    }
}

class MotorcycleWithSidecar extends Motorcycle {

    private final String passengerName;

    public MotorcycleWithSidecar(String driverName, String passengerName) {
        super(driverName);
        this.passengerName = passengerName;
    }

    public void doAControlledSkid() {
        System.out.println("Are you mad ?");
    }
}

public class Inheritance {
    public static void main(String[] args) {

        // Inheritance relationships also define _subtypes_. We can store instances of a child class in a variable that has the base
        // class as its type.
        // However, if we do this we can't call the methods defined in the child class.
        Vehicle v1 = new Motorcycle("Benoit");
        Vehicle v2 = new Car("Jacquemine");
        Vehicle v3 = new Vehicle("Norbert");
        Vehicle v4 = new MotorcycleWithSidecar("Norbert", "Jacquemine");

        // When we call this method, the accelerate method of the Car class is called, even though the variable is of type Vehicle. This is
        // because the object inside the variable is still an instance of the Car class.
        v1.accelerate();
        v1.accelerate();
        System.out.println(v1.getCurrentSpeed());

        v2.accelerate();
        v2.accelerate();
        System.out.println(v2.getCurrentSpeed());

        v3.accelerate();
        v3.accelerate();
        System.out.println(v3.getCurrentSpeed());

        v4.accelerate();
        v4.accelerate();
        System.out.println(v4.getCurrentSpeed());

        // To access the child class methods, we can _cast_ the variable. However, it will fail is the instance is not of the correct type.
        if (v1 instanceof Vehicle) {
            System.out.println("v1 is Vehicle");
        }
        // Verify instance
        if (v1 instanceof Motorcycle m) {
            System.out.println("vi is Motorcycle");
            m.accelerate();
            m.doAControlledSkid();
        }
        // Cast is similar but it can produce ClassCastException
        Motorcycle m = (Motorcycle) v1;
        m.accelerate();
        m.doAControlledSkid();
        //System.out.println(m.getCurrentSpeed());
        // There is still only one instance of the motorcylcle class, even though it's stored in two different variables.
        System.out.println(v1.getCurrentSpeed());
    }
}
