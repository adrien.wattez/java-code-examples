package language;

import java.util.Scanner;

// In the programmation world, programs always have a _standard input_ and a _standard output_. The program can send text to the standard
// output and receive text from the standard input.
// What this text becomes or where it comes from depends on how the program is executed, and the program itself doesn't care.
// When a program is executed with the command line, standard output is printed to the console, and the standard input receives the text
// that is typed into the console.
// Like most programming languages, java has a way to interact with standard output and input.
// `System.out` contains an object representing the standard output, and `System.in` contains an object representing the standard input.
public class StandardInputAndOutput {

    public static void main(String[] args) {
        // the most straightforward way to end text to the standard output is to use the System.out.println method.
        System.out.println("Hello !");

        // Same, but with no carriage return at the end of the line.
        System.out.print("Hello ");

        // A simple way to read text from the standard input is to use the Scanner class.
        Scanner s = new Scanner(System.in);
        System.out.println("What's your name ?");

        // The nextLine method of the Scanner class will wait for the next line and return it.
        String name = s.nextLine();
        System.out.println("Nice to meet you, " + name);

        // Be careful, Scanner.nextLine always returns a String. If you want another type, you need to convert the string into it
        System.out.println("How old are you ?");
        String ageString = s.nextLine();
        int age = Integer.parseInt(ageString);

        // It is recommended to use the other methods of Scanner, they will do this conversion for you and provide additional checks.
        System.out.println("What's the age of the captain?");
        int captainAge = s.nextInt();
        System.out.println("Are you sure the captain is really " + captainAge + " years old ?");
    }

}
