package language;

// This is an _enum_. It's a special kind of class : you can't create new instances of an enum. Instead, there are a fixed number of
// instances that each have a different name.
// This means that enums are useful to represent types that have a fixed number of different values, like this example (days of a week)
enum DayOfWeek {
    MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
}

// Enums are still classes, so that can have methods and attributes as usual. However, their constructor are only called once for each enum
// constant. It can't be called from anywhere in the code.
enum Criticity {
    LOW(0), MEDIUM(1), HIGH(2);

    public final int criticityCode;

    Criticity(int criticityCode) {
        this.criticityCode = criticityCode;
    }
}

public class Enum {
    public static void main(String[] args) {

        // enum values can't be constructed, instead we get them by their name.
        DayOfWeek startOfWeekend = DayOfWeek.SATURDAY;
        System.out.println("The weekend starts on "+startOfWeekend);

        // When we have a value of an enum type, we can call methods or access attributes as usual.
        Criticity nuclearPowerPlantExplosion = Criticity.HIGH;
        System.out.println(nuclearPowerPlantExplosion.criticityCode);

        // We can iterate on all the values of a given enum.
        for (DayOfWeek d : DayOfWeek.values()) {
            System.out.println(d);
        }
    }
}
