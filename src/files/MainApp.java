package files;

import java.io.File;
import java.io.IOException;

public class MainApp {
    /**
     * FILENAME is the path to find the file 'animals.txt'. This file is in the same folder than this .java
     */
    public static final String FILENAME = new File("src/files/animals.txt").getAbsolutePath();

    /**
     * Here is simply the method to execute the reading file.
     */

    public static void main(String[] args) throws IOException {
        ReadFile rf = new ReadFile();
        System.out.println("Reading file with FileReader :" + System.lineSeparator());
        rf.displayAnimalsWithFileReader(FILENAME);

        System.out.println(System.lineSeparator() + "Reading file with BufferReader :" + System.lineSeparator());
        rf.displayAnimalsWithBufferReader(FILENAME);

        System.out.println(System.lineSeparator() + "Reading file with Scanner :" + System.lineSeparator());
        rf.displayAnimalsWithScanner(FILENAME);
    }
}
